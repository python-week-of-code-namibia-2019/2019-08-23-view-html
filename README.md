# Python Week Of Code, Namibia 2019

To launch the Django web server,
run

```
python manage.py runserver
```

We will ignore

```
You have 14 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
```

for now.

Open [http://localhost:8000/](http://localhost:8000/) in your web browser.

[blog/templates/blog/index.html](blog/templates/blog/index.html) has the source code of the page that you see in your web browser.

## Tasks for Instructor

1. Change the content of the element `<h1>`.
2. Change the content of the element `<p>`.
3. Change the content of the element `<title>`.
4. Add

   ```
   <a href="<a href="https://docs.djangoproject.com/">Django</a>
   ```

   inside the element `<p>`.
5. Add

  ```
   <input type="color" name="favcolor" value="#ff0000">
  ```

  inside the element `<body>`.

## Tasks for Learners

1. Add a new element `<p>`.
2. Add a new element `<h2>`.
3. Add a new element `<h3>`.
4. Add a new element `<h4>`.
5. Change the content of the element `<p>`.
6. Change the content of the element `<title>`.
7. Change the element `<input>` to

   ```
   <input type="range" name="points" min="0" max="10">
   ```